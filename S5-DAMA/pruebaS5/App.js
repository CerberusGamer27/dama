import React, { Component } from 'react';
import {
  NativeBaseProvider, Button, Input, VStack, Heading, Box,
  FormControl,
  HStack,
  Avatar,
  InputGroup,
  InputLeftAddon,
  ScrollView,
} from 'native-base';
import { TextInput, Text, Alert, StyleSheet, ImageBackground } from 'react-native';
import * as yup from 'yup'
import { Formik } from 'formik'
import { paddingTop } from 'styled-system';

const image = { uri: "https://img.wallpapersafari.com/phone/1080/1920/0/16/2EvYKa.jpg" };

const showAlert = (values) => {
  alert("Resultado de Formulario: \n\nNombre: " + values.name + "\nApellido: " + values.apellido + "\nEdad: " + values.edad
    + "\nCorreo: " + values.email + "\nDirección: " + values.direccion + "\nPromedio: " + values.promedio);
}


function Example() {
  return <Formik
    initialValues={{
      name: '',
      apellido: '',
      edad: '',
      email: '',
      telefono: '',
      direccion: '',
      promedio: '',
    }}
    onSubmit={values => showAlert(values)}
    validationSchema={yup.object().shape({
      name: yup
        .string()
        .required('Por favor, Digite su Nombre'),
      apellido: yup
        .string()
        .required('Por favor, Digite su Apellido'),
      edad: yup
        .number()
        .required('Por favor, Digite su Edad'),
      email: yup
        .string()
        .email('Ingrese un correo valido')
        .required('Por favor, Digite su Correo'),
      telefono: yup
        .number()
        .required('Por favor, Digite su Telefono'),
      direccion: yup
        .string()
        .required('Por favor, Digite su Direccion'),
      promedio: yup
        .number()
        .required('Por favor, Digite su Promedio'),
    })}>
    {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
      <Box SafeArea flex={1} p={2} w="90%" h="100%" mt='50' mx='auto'>
        <HStack space={2} mt={5} justifyContent="space-between">
          <Heading size="md" bold color={"white"}>
            Bienvenido Alumno
          </Heading>
          <Avatar size="xl" source={{
            uri: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/81c743dd-60e4-49b6-91ed-d6849a6491bc/daepq9j-b624be25-310e-48fe-aa3a-7a8a10284c87.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzgxYzc0M2RkLTYwZTQtNDliNi05MWVkLWQ2ODQ5YTY0OTFiY1wvZGFlcHE5ai1iNjI0YmUyNS0zMTBlLTQ4ZmUtYWEzYS03YThhMTAyODRjODcuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.d7yM98GG-CA84EbIrKqo2PelFaql35NNojDI9Wna5r4"
          }}>
            GG
            <Avatar.Badge bg={"green.500"} />
          </Avatar>
        </HStack>
        <VStack space={2} fontSize="xs" mt={5}>
          <FormControl isInvalid={'name' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Nombre
            </FormControl.Label>
            <Input type='text' variant={"rounded"}
              value={values.name}
              onChangeText={handleChange('name')}
              onBlur={() => setFieldTouched('name')}
              placeholder="Tu nombre Aqui" />
            {touched.name && errors.name &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.name}</Text>
            }
          </FormControl>

          <FormControl isInvalid={'apellido' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Apellido
            </FormControl.Label>
            <Input type='text' variant={"rounded"}
              value={values.apellido}
              onChangeText={handleChange('apellido')}
              onBlur={() => setFieldTouched('apellido')}
              placeholder="Tu Apellido Aqui" />
            {touched.apellido && errors.apellido &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.apellido}</Text>
            }
          </FormControl>

          <FormControl isInvalid={'edad' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Edad
            </FormControl.Label>
            <Input keyboardType='numeric' variant={"rounded"}
              value={values.edad}
              onChangeText={handleChange('edad')}
              onBlur={() => setFieldTouched('edad')}
              placeholder="Tu Edad Aqui" />
            {touched.edad && errors.edad &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.edad}</Text>
            }
          </FormControl>

          <FormControl isInvalid={'email' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Correo
            </FormControl.Label>
            <Input type='text' variant={"rounded"} keyboardType='email-address'
              value={values.email}
              onChangeText={handleChange('email')}
              onBlur={() => setFieldTouched('email')}
              placeholder="Tu correo aqui" />
            {touched.email && errors.email &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.email}</Text>
            }
          </FormControl>

          <FormControl isInvalid={'telefono' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Telefono
            </FormControl.Label>
            <Input keyboardType='phone-pad' variant={"rounded"}
              value={values.telefono}
              onChangeText={handleChange('telefono')}
              onBlur={() => setFieldTouched('telefono')}
              placeholder="Tu Telefono Aqui" />
            {touched.telefono && errors.telefono &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.telefono}</Text>
            }
          </FormControl>

          <FormControl isInvalid={'direccion' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Direccion
            </FormControl.Label>
            <Input type='text' variant={"rounded"}
              value={values.direccion}
              onChangeText={handleChange('direccion')}
              onBlur={() => setFieldTouched('direccion')}
              placeholder="Tu Direccion Aqui" />
            {touched.direccion && errors.direccion &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.direccion}</Text>
            }
          </FormControl>

          <FormControl isInvalid={'promedio' in errors}>
            <FormControl.Label _text={{ color: 'white', fontSize: 'sm', fontWeight: 600 }}>
              Promedio
            </FormControl.Label>
            <Input keyboardType='numeric' variant={"rounded"}
              value={values.promedio}
              onChangeText={handleChange('promedio')}
              onBlur={() => setFieldTouched('promedio')}
              placeholder="Tu Promedio Aqui" />
            {touched.promedio && errors.promedio &&
              <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.promedio}</Text>
            }
          </FormControl>

          <VStack space={1} mt='20' mb='5'>
            <Button colorScheme="cyan" _text={{ color: 'white' }} disabled={!isValid} onPress={handleSubmit}>Registrar</Button>
          </VStack>
        </VStack>
      </Box>
    )}
  </Formik>
}

const styles = StyleSheet.create({

  image: {
    flex: 1,
    resizeMode: 'cover',
  }
});

export default function App() {
  return (
    <NativeBaseProvider>
        <ScrollView>
        <ImageBackground source={image} resizeMode="cover" style={styles.image}>
          <Example />
        </ImageBackground>
        </ScrollView>
      </NativeBaseProvider>
  );
}