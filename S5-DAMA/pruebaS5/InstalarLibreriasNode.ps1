Write-Output "`n Instalando Formik... `n"
npm install formik --save
Write-Output "`n Formik Instalado Correctamente `n"
Write-Output "`n `n Configurando los Permisos... `n"
npm config set legacy-peer-deps true
Write-Output "`n Permisos Configurados Correctamente `n"
Write-Output "`n `n Instalando native-base e styled-components... `n"
npm install native-base styled-components styled-system
Write-Output "`n Native-Base y styled-components Instalados Correctamente `n"
Write-Output "`n `n Instalando react-native-svg... `n"
expo install react-native-svg
Write-Output "`n `n Instalando react-native-area-context... `n"
expo install react-native-safe-area-context
Write-Output "`n `n Instalando expo-font... `n"
expo install expo-font
Write-Output "`n `n Instalando yup... `n"
npm install yup --save
Write-Output "`n `n yup Instalado correctamente `n"
Write-Output "`n `n TODOS LOS PAQUETES Y PERMISOS HAN SIDO CONFIGURADOS DE MANERA CORRECTA `n"