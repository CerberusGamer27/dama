Param(
    
    [string] $name
)
If($PSBoundParameters.ContainsKey("name")) {
    Write-Output "`nIniciando Proyecto con el nombre $name"
    expo init $name --template @native-base/expo-template
    Write-Output "`nCambiando al directorio del proyecto $name"
    Set-Location .\$name
    Write-Output "`n---INICIANDO INSTALACION DE PAQUETES----`n"
    Write-Output "`nInstalando Formik... `n"
    npm install formik --save
    Write-Output "`nFormik Instalado Correctamente `n"
    Write-Output "`n`nConfigurando los Permisos Node... `n"
    npm config set legacy-peer-deps true
    Write-Output "`nPermisos Configurados Correctamente `n"
    Write-Output "`n`nInstalando styled-components... `n"
    npm install styled-components styled-system
    Write-Output "`nstyled-components instalado correctamente `n"
    Write-Output "`n`nInstalando expo-font... `n"
    expo install expo-font
    Write-Output "`n`nInstalando yup... `n"
    npm install yup --save
    Write-Output "`n`nyup Instalado correctamente `n"
    Write-Output "`n`nRunning expo doctor --fix-dependencies "
    expo doctor --fix-dependencies
    Write-Output "`n`nTODOS LOS PAQUETES Y PERMISOS HAN SIDO CONFIGURADOS DE MANERA CORRECTA `n"
    Write-Output "`n`nIniciando Proyecto `n`n"
    expo start
}
else {
    Write-Host "Necesitas pasar el parametro -name nombreProyecto `n"
    Write-Host "Ejemplo de Ejecucion: `n`n .\scriptNative.ps1 -name proyectoPrueba"
}

