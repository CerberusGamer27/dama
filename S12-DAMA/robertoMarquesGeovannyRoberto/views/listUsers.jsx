import React, { useEffect, useState } from 'react'
import {
    NativeBaseProvider,
    Box,
    Center,
    HStack,
    VStack,
    Heading,
    Text,
    Spacer,
    FlatList
} from "native-base"
import axios from 'axios'

const listUsers = () => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        const obtenerUsuarios = async () => {
            const url = 'http://localhost/react-native-web/listUsers.php';
            const { data } = await axios.get(url);
            setUsers(data);
        }
        obtenerUsuarios();
    }, [])

    console.log(users);

    return (
        <NativeBaseProvider>
            <Box w={"95%"} mt={5} flex={1} p={1}>
                <Heading fontSize="xl" p="4" pb="3" textAlign={'center'}>
                    Listado de Usuarios
                </Heading>
                <FlatList data={users} renderItem={({ item }) =>
                    <Box borderBottomWidth="1" _dark={{ borderColor: "muted.50" }}
                        borderColor="muted.800" pl={["0", "4"]} pr={["0", "5"]} py="2">
                        <HStack space={2} justifyContent="space-between">
                            <VStack>
                                <Text _dark={{ color: "warmGray.50" }} color="coolGray.800" bold>
                                    ID Usuario: {item.id}
                                </Text>
                                <Text color="coolGray.600" _dark={{ color: "warmGray.200" }}>
                                    <Text italic bold>Nombre: </Text>{item.name}
                                </Text>
                                <Text color="coolGray.600" _dark={{ color: "warmGray.200" }}>
                                    <Text italic bold>Email: </Text>{item.email}
                                </Text>
                                <Text color="coolGray.600" _dark={{ color: "warmGray.200" }}>
                                    <Text italic bold>Password: </Text>{item.password}
                                </Text>
                            </VStack>
                            <Spacer />
                        </HStack>
                    </Box>} keyExtractor={item => item.id} />
            </Box>
        </NativeBaseProvider>
    )
}

export default listUsers