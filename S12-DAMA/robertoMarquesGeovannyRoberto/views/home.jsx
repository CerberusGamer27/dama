import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class Home extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.pageName}>Gestion de Usuarios Geovanny Martinez Roberto Otoniel</Text>

                <TouchableOpacity
                    onPress={() => navigate('Login')} style={styles.btn1}>
                    <Text style={styles.btnText}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigate('Register')}
                    style={styles.btn2}>
                    <Text style={styles.btnText}>Register</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigate('listUsers')}
                    style={styles.btn3}>
                    <Text style={styles.btnText}>Listar Usuario</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn1: {
        backgroundColor: 'orange',
        padding: 10,
        margin: 10,
        width: '95%',
    },
    btn2: {
        backgroundColor: 'blue',
        padding: 10,
        margin: 10,
        width: '95%',
    },
    btn3: {
        backgroundColor: 'rgb(77, 103, 145)',
        padding: 10,
        margin: 10,
        width: '95%',
    },
    pageName: {
        margin: 10,
        fontWeight: 'bold',
        color: '#000',
        textAlign: 'center',
    },
    btnText: {
        color: '#fff',
        fontWeight: 'bold',
    },
});
