import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from './views/home';
import Login from './views/login';
import Register from './views/register';
import Profile from './views/profile';
import listUsers from './views/listUsers';


const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            // headerShown: false, // if not gonna show header
          }}
          >
          <Stack.Screen name="HomeScreen" component={Home} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="listUsers" component={listUsers} options={{title: "Listar a los usuarios"}} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
};

export default App;
