import {ScrollView} from 'react-native';
import {
    NativeBaseProvider, Text, VStack, Heading, Box, HStack, Avatar,
    WarningOutlineIcon
} from 'native-base';
const imagenFondo = {uri:"https://img.wallpapersafari.com/phone/1080/1920/0/16/2EvYKa.jpg"}
export default function Result({navigation, route}) {

    const {
        nombres,
        apellidos,
        telefono, 
        edad, 
        correo, 
        direccion, 
        sueldo,
        afp,
        isss,
        renta
    } = route.params;

    return (
        <NativeBaseProvider>
            {/* <ImageBackground source={imagenFondo} resizeMode="cover" style={{ flex: 1, justifyContent: "center" }}> */}
                <ScrollView >
                    <Box mt={5} flex={1} p={1} w="95%" mx='auto'>
                        <HStack justifyContent="space-between" space={2} alignItems={"center"}>
                            <Heading style={styles.tituloForm} size="sm">{nombres + ' ' + apellidos}</Heading>
                            <Avatar source={{ uri: "https://img1.ak.crunchyroll.com/i/spire4/be7ccab083087be99884531cadd7d5651630065450_large.png" }} size="xl" justifyContent="center">
                                <Avatar.Badge bg={"green.500"} />
                            </Avatar>
                        </HStack>
                        <Heading size={'md'} mb='5'>Datos del Empleado</Heading>
                        <VStack space={8} backgroundColor='blue.100' pt={'5'} pb={'5'} rounded={'25'} alignItems="flex-start">
                            <Text paddingLeft={'10'}><Text bold>Telefono </Text>: {telefono}</Text>
                            <Text paddingLeft={'10'}><Text bold>Edad </Text>: {edad}</Text>
                            <Text paddingLeft={'10'}><Text bold>Correo </Text>: {correo}</Text>
                            <Text paddingLeft={'10'}><Text bold>Direccion </Text>: {direccion}</Text>
                            <Text paddingLeft={'10'}><Text bold>Sueldo </Text>: {sueldo}</Text>
                            <Text paddingLeft={'10'}><Text bold>AFP </Text>: {afp}</Text>
                            <Text paddingLeft={'10'}><Text bold>ISSS </Text>: {isss}</Text>
                            <Text paddingLeft={'10'}><Text bold>RENTA </Text>: {renta}</Text>

                        </VStack>
                    </Box>
                </ScrollView>
            {/* </ImageBackground> */}
        </NativeBaseProvider>
    );
}

const styles = {
    inputSeleccionado: {
        bg: "coolGray.200:alpha.100"
    },
    botonDisabled: {
        backgroundColor: '#00aeef'
    },
    labelInput: {
        color: 'white',
        fontSize: 'sm',
        fontWeight: 'bold'
    },
    tituloForm: {
        color: '#8796FF'
    }
}