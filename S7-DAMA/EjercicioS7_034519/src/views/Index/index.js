import { StyleSheet, ScrollView, SafeAreaView, StatusBar, View, Alert, ImageBackground } from 'react-native';
import {
    NativeBaseProvider, Image, Button, Text, Input, VStack, Heading, Box, FormControl, HStack, Avatar, InputGroup, InputLeftAddon,
    WarningOutlineIcon
} from 'native-base';
import * as yup from 'yup';
import { Formik } from 'formik';

export default function Index({navigation}) {

    const sendInfo = (values) => {
        const { nombre1, nombre2, apellidos1, apellidos2, telefono, edad, correo, direccion, sueldo} = values;
        const AFP = 0.10;
        const ISSS = 0.035;
        const RENTA = 0.625;
        navigation.navigate('Result', {
            nombres: `${nombre1} ${nombre2}`,
            apellidos: `${apellidos2} ${apellidos1}`,
            telefono,
            edad,
            correo,
            direccion,
            sueldo,
            afp: (sueldo * AFP).toFixed(2),
            isss: (sueldo * ISSS).toFixed(2),
            renta: (sueldo * RENTA).toFixed(2),
        })
    }

    const imagenFondo = { uri: "https://img.wallpapersafari.com/phone/1080/1920/0/16/2EvYKa.jpg" }
    const regexTelefono = /\D*(\d{4})\D*(\d{4})/;
    const formularioEsquemaValidacion = yup.object().shape({
        nombre1: yup.string()
            .required("Digite el nombre del empleado")
            .min(2, "El nombre ingresado es demasiado corto"),
        nombre2: yup.string()
            .required("Digite el nombre del empleado")
            .min(2, "El nombre ingresado es demasiado corto"),
        apellidos1: yup.string()
            .required("Digite los apellidos del empleado")
            .min(2, "Los apellidos ingresados son demasiados cortos"),
        apellidos2: yup.string()
            .required("Digite los apellidos del empleado")
            .min(2, "Los apellidos ingresados son demasiados cortos"),
        edad: yup.number()
            .min(18, "La edad minima es de 18 años")
            .max(100, "La edad maxima es de 100 años")
            .required("Ingrese la edad del empleado"),
        correo: yup.string()
            .email("Ingrese una direccion de correo valida")
            .required("Introduzca la direccion de correo del empleado"),
        telefono: yup.string()
            .required("Ingrese un numero telefonico valido")
            .matches(regexTelefono, "Ingrese un numero con el siguiente formato ####-####")
            .min(8, "El numero telefonico ingresado es muy corto")
            .max(8, "El numero ingresado es muy largo"),
        direccion: yup.string()
            .required("Ingrese una direccion valida")
            .min(10, "La direccion ingresado es muy corta"),
        sueldo: yup.number()
            .required("Ingrese el salario del empleado")
            .min(0, "El salario debe ser mayor a $0.00")
    })

    return (
        <Formik
            initialValues={{
                nombre1: '',
                nombre2: '',
                apellidos1: '',
                apellidos2: '',
                edad: '18',
                correo: '',
                telefono: '',
                direccion: '',
                sueldo: ''
            }}
            onSubmit={values => sendInfo(values)}
            validationSchema={formularioEsquemaValidacion}>
            {({
                handleChange,
                handleSubmit,
                values,
                errors,
                touched,
                setFieldTouched
            }) => (
                <View style={{ flex: 1}}>
                    <NativeBaseProvider>
                        {/* <ImageBackground source={imagenFondo} resizeMode="cover" style={{ flex: 1, justifyContent: "center" }}> */}
                            <ScrollView >
                                <Box mt={5} flex={1} p={1} w="95%" mx='auto'>
                                    <HStack justifyContent="space-between" space={2} alignItems={"center"}>
                                        <Heading style={styles.tituloForm} size="xl">Fomulario</Heading>
                                        <Avatar source={{ uri: "https://img1.ak.crunchyroll.com/i/spire4/be7ccab083087be99884531cadd7d5651630065450_large.png" }} size="xl" justifyContent="center">
                                            <Avatar.Badge bg={"green.500"} />
                                        </Avatar>
                                    </HStack>

                                    <VStack space={8}>
                                        <FormControl isInvalid={'nombre1' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Primer Nombre</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite un nombre'
                                                value={values.nombre1}
                                                onChangeText={handleChange('nombre1')}
                                                onBlur={() => setFieldTouched('nombre1')} />
                                            {touched.nombre1 && errors.nombre1 &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.nombre1}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <FormControl isInvalid={'nombre2' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Segundo Nombre</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite un nombre'
                                                value={values.nombre2}
                                                onChangeText={handleChange('nombre2')}
                                                onBlur={() => setFieldTouched('nombre2')} />
                                            {touched.nombre2 && errors.nombre2 &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.nombre2}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <FormControl isInvalid={'apellidos1' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Apellidos</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite un apellido'
                                                value={values.apellidos1}
                                                onChangeText={handleChange('apellidos1')}
                                                onBlur={() => setFieldTouched('apellidos1')} />
                                            {touched.apellidos1 && errors.apellidos1 &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.apellidos1}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <FormControl isInvalid={'apellidos2' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Apellidos</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite un apellido'
                                                value={values.apellidos2}
                                                onChangeText={handleChange('apellidos2')}
                                                onBlur={() => setFieldTouched('apellidos2')} />
                                            {touched.apellidos2 && errors.apellidos2 &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.apellidos2}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <FormControl isInvalid={'edad' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Edad</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite la edad del empleado' keyboardType='number-pad'
                                                value={values.edad}
                                                onChangeText={handleChange('edad')}
                                                onBlur={() => setFieldTouched('edad')} />
                                            {touched.edad && errors.edad &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.edad}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <FormControl isInvalid={'correo' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Correo Electronico</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='ejemplo@ejemplo.com' keyboardType='email-address'
                                                value={values.correo}
                                                onChangeText={handleChange('correo')}
                                                onBlur={() => setFieldTouched('correo')} />
                                            {touched.correo && errors.correo &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.correo}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <FormControl isInvalid={'telefono' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Telefono</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='00000000' keyboardType='phone-pad'
                                                value={values.telefono}
                                                onChangeText={handleChange('telefono')}
                                                onBlur={() => setFieldTouched('telefono')} />
                                            {touched.telefono && errors.telefono &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.telefono}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>

                                        <FormControl isInvalid={'direccion' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Direccion</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite la direccion del empleado'
                                                value={values.direccion}
                                                onChangeText={handleChange('direccion')}
                                                onBlur={() => setFieldTouched('direccion')} />
                                            {touched.direccion && errors.direccion &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.direccion}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>

                                        <FormControl isInvalid={'sueldo' in errors}>
                                            <FormControl.Label _text={styles.labelInput}>Sueldo</FormControl.Label>
                                            <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Ingrese el sueldo del empleado' keyboardType='decimal-pad'
                                                value={values.sueldo}
                                                onChangeText={handleChange('sueldo')}
                                                onBlur={() => setFieldTouched('sueldo')} />
                                            {touched.sueldo && errors.sueldo &&
                                                <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                                    {errors.sueldo}
                                                </FormControl.ErrorMessage>
                                            }
                                        </FormControl>
                                        <Button mt={5} _disabled={styles.botonDisabled} onPress={handleSubmit} colorScheme="green" _text={{ color: 'white' }} mb={4}>Registrar</Button>
                                    </VStack>
                                </Box>
                            </ScrollView>
                        {/* </ImageBackground> */}
                    </NativeBaseProvider>
                </View>
            )}
        </Formik>
    );
}

const styles = {
    inputSeleccionado: {
        bg: "coolGray.200:alpha.100"
    },
    botonDisabled: {
        backgroundColor: '#00aeef'
    },
    labelInput: {
        color: 'black',
        fontSize: 'sm',
        fontWeight: 'bold'
    },
    tituloForm: {
        color: '#8796FF'
    }
}