import React from 'react';
import { NativeBaseProvider, VStack, Box, Heading, FlatList, Avatar, Spacer, Center, AspectRatio, Image, Stack, HStack, StyleSheet, Text, View, Button } from 'native-base';

export default function Estudios() {
    const estudios = [{
        id: "1",
        lugar: "Colegio Ceren",
        tipo: "Bachillerato",
        estudio: "Bachillerato en Electronica",
        inicio: "2016",
        final: "2018",
    }, {
        id: "2",
        lugar: "ITCA-FEPADE",
        tipo: "Universidad",
        estudio: "Ingeniería de Software",
        inicio: "2019",
        final: "Actual",
    }, {
        id: "3",
        lugar: "Google:",
        tipo: "Curso",
        estudio: "GCP Fundamentals",
        inicio: "Ago 2020",
        final: "Dec 2020",
    }, {
        id: "4",
        lugar: "UNAM",
        tipo: "Curso",
        estudio: "Razonamiento artificial",
        inicio: "Oct 2020",
        final: "Dic 2020",
    }, {
        id: "5",
        lugar: "Secretaria de Innovacion",
        tipo: "Curso",
        estudio: "Introduccion al ML",
        inicio: "Dic 2020",
        final: "Ene 2021",
    }, {
        id: "6",
        lugar: "Secretaria de Innovacion",
        tipo: "Curso",
        estudio: "Introduccion Ciberseguridad",
        inicio: "Jun 2021",
        final: "Jul 2021",
    }, {
        id: "7",
        lugar: "Udemy",
        tipo: "Curso",
        estudio: "Introduccion a JS Moderno",
        inicio: "2022",
        final: "Actual",
    }
];
    return (
        <NativeBaseProvider>
            <Box w={"95%"} mt={5} flex={1} p={1}>
                <Heading fontSize="xl" p="4" pb="3" textAlign={'center'}>
                    Estudios y Cursos Realizados
                </Heading>
                <FlatList data={estudios} renderItem={({ item }) =>
                    <Box borderBottomWidth="1" _dark={{borderColor: "muted.50"}} 
                        borderColor="muted.800" pl={["0", "4"]} pr={["0", "5"]} py="2">
                        <HStack space={2} justifyContent="space-between">
                            <VStack>
                                <Text _dark={{color: "warmGray.50"}} color="coolGray.800" bold>
                                    {item.lugar}
                                </Text>
                                <Text color="coolGray.600" _dark={{ color: "warmGray.200" }}>
                                    <Text italic>{item.tipo} : </Text> {item.estudio}
                                </Text>
                            </VStack>
                            <Spacer />
                            <Text fontSize="xs" _dark={{
                                color: "warmGray.50"
                            }} color="coolGray.800" alignSelf="flex-start">
                                {item.inicio} - {item.final} 
                            </Text>
                        </HStack>
                    </Box>} keyExtractor={item => item.id} />
            </Box>
        </NativeBaseProvider>
    )
}