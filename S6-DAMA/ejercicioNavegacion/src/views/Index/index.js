import React from 'react';
import {
    NativeBaseProvider, VStack, ZStack, Stack, Center, Box, Heading,
    Divider, StyleSheet, Text, View, Button
} from 'native-base';

export default function Index({ navigation }) {
    return (
        <NativeBaseProvider>
            <Box mt={5} flex={1} p={1} w="95%" mx='auto' justifyContent={'center'} bg={{
                linearGradient: {
                    colors: ['lightBlue.300', 'violet.800'],
                    start: [0, 0],
                    end: [1, 0]
                }
            }}>
                <VStack space={2} px="2" alignItems="center" bg={'amber.100'} rounded='50' justifyContent="center">
                    <Heading size="md" pt={'3'}>Menu Estudiante</Heading>
                    <Stack mb="2.5" mt="1.5" direction={{
                        base: "column",
                        md: "row"
                    }} space={2} mx={{
                        base: "auto",
                        md: "0"
                    }}>
                        <Button size="lg" variant="outline" onPress={() => navigation.navigate('Informacion')}>Informacion</Button>
                        <Button size="lg" variant="outline" onPress={() => navigation.navigate('Estudios')}>
                            Estudios
                        </Button>
                    </Stack>

                </VStack>

            </Box>
        </NativeBaseProvider>
    )
}

// const styles = StyleSheet.create({
//     linearGradient: {
//         colors: ['lightBlue.300', 'violet.800'],
//         start: [0, 0],
//         end: [1, 0]
//     }
// })

