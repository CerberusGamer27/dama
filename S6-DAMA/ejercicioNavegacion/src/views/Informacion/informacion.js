import React from 'react';
import { NativeBaseProvider, VStack, Box, Heading, Center, StyleSheet, Text, View, Button } from 'native-base';

export default function Informacion(){
    return(
        <NativeBaseProvider>
            <Box mt={5} flex={1} p={1} w="95%" mx='auto'>
                        <Heading size={'md'} mb='5'>Datos del Estudiante</Heading>
                        <VStack space={8} backgroundColor='blue.100' pt={'5'} pb={'5'} rounded={'25'} alignItems="flex-start">
                            <Text paddingLeft={'10'}><Text bold>Nombre </Text>: Geovanny Martinez</Text>
                            <Text paddingLeft={'10'}><Text bold>Carnet </Text>: 031519</Text>
                            <Text paddingLeft={'10'}><Text bold>Edad </Text>: 21</Text>
                            <Text paddingLeft={'10'}><Text bold>Correo </Text>: geovanny.martinez19@itca.edu.sv</Text>
                            <Text paddingLeft={'10'}><Text bold>Direccion </Text>: Armenia</Text>
                        </VStack>
                    </Box>
        </NativeBaseProvider>
    )
}