import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Index from './src/views/Index/index';
import Informacion from './src/views/Informacion/informacion';
import Estudios from './src/views/Estudios/estudios';


const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name ="Index" component={Index} />
        <Stack.Screen name ="Informacion" component={Informacion} />
        <Stack.Screen name ="Estudios" component={Estudios} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}