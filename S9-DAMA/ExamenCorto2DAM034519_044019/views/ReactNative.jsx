import { React } from "react";
import { Box, AspectRatio, Center, Stack, Heading, Text, HStack, useTheme, NativeBaseProvider, Image } from "native-base";

const ReactNative = () => {
    return (
        <NativeBaseProvider>
            <Box alignItems="center">
                <Box maxW="80" rounded="lg" overflow="hidden" paddingTop={1} borderColor="coolGray.200" borderWidth="1" _dark={{
                    borderColor: "coolGray.600",
                    backgroundColor: "gray.700"
                }} _web={{
                    shadow: 2,
                    borderWidth: 0
                }} _light={{
                    backgroundColor: "gray.50"
                }}>
                    <Box>
                        <AspectRatio w="100%" ratio={16 / 9}>
                            <Image source={{
                                uri: "https://reactjs.org/logo-og.png"
                            }} alt="image" />
                        </AspectRatio>
                    </Box>
                    <Stack p="4" space={3}>
                        <Stack space={2}>
                            <Heading size="md" ml="-1">
                                React Native
                            </Heading>
                            <Text fontSize="xs" _light={{
                                color: "violet.500"
                            }} _dark={{
                                color: "violet.400"
                            }} fontWeight="500" ml="-0.5" mt="-1">
                                Framework de Código Abierto
                            </Text>
                        </Stack>
                        <Text fontWeight="400">
                            Se utiliza para desarrollar aplicaciones para Android, Android TV, iOS, macOS, tvOS, Web, Windows y UWP 
                            al permitir que los desarrolladores usen React con las características nativas de estas plataformas.
                            También se utiliza para desarrollar aplicaciones de realidad virtual con Oculus.
                        </Text>
                    </Stack>
                </Box>
            </Box>
        </NativeBaseProvider>
    )
};

export default ReactNative;