import React from 'react';
import {
    NativeBaseProvider, VStack, ZStack, Stack, Center, Box, Heading,
    Divider, StyleSheet, Text, View, Button, ScrollView, FormControl
    , InputGroup, InputLeftAddon, WarningOutlineIcon, Input
} from 'native-base';

import * as yup from 'yup';
import { Formik } from 'formik';

export default function InformacionPersonal({ navigation }) {


    const formularioEsquemaValidacion = yup.object().shape({
        nombre1: yup.string()
            .required("Digite el nombre del Alumno 1")
            .min(2, "El nombre ingresado es demasiado corto"),
        nombre2: yup.string()
            .required("Digite el nombre del Alumno 1")
            .min(2, "El nombre ingresado es demasiado corto"),
        carnet1: yup.string()
            .required("Digite el carnet del Alumno 1")
            .min(2, "El carnet es muy corto"),
        carnet2: yup.string()
            .required("Digite el carnet del Alumno 1")
            .min(2, "El carnet es muy cortos"),
    })

    const sendInfo = (values) => {
        const { nombre1, nombre2, carnet1, carnet2 } = values;
        navigation.navigate('MostrarInfo', {
            nombre1,
            nombre2,
            carnet1,
            carnet2
        });
    }
    return (
        <NativeBaseProvider>
            <ScrollView>
                <Box mt={5} flex={1} p={1} w="95%" mx='auto' justifyContent={'center'}>
                    <Heading size="md">Informacion Alumnos</Heading>
                    <Formik
                        initialValues={{
                            nombre1: '',
                            nombre2: '',
                            carnet1: '',
                            carnet2: '',
                        }}
                        onSubmit={values => sendInfo(values)}
                        validationSchema={formularioEsquemaValidacion}>
                        {({
                            handleChange,
                            handleSubmit,
                            values,
                            errors,
                            touched,
                            setFieldTouched
                        }) => (

                            <VStack space={8}>
                                <FormControl isInvalid={'nombre1' in errors}>
                                    <FormControl.Label _text={styles.labelInput}>Nombre Alumno 1</FormControl.Label>
                                    <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite un nombre'
                                        value={values.nombre1}
                                        onChangeText={handleChange('nombre1')}
                                        onBlur={() => setFieldTouched('nombre1')} />
                                    {touched.nombre1 && errors.nombre1 &&
                                        <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                            {errors.nombre1}
                                        </FormControl.ErrorMessage>
                                    }
                                </FormControl>
                                <FormControl isInvalid={'nombre2' in errors}>
                                    <FormControl.Label _text={styles.labelInput}>Nombre Alumno 2</FormControl.Label>
                                    <Input _focus={styles.inputSeleccionado} variant="filled" placeholder='Digite un nombre'
                                        value={values.nombre2}
                                        onChangeText={handleChange('nombre2')}
                                        onBlur={() => setFieldTouched('nombre2')} />
                                    {touched.nombre2 && errors.nombre2 &&
                                        <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                            {errors.nombre2}
                                        </FormControl.ErrorMessage>
                                    }
                                </FormControl>
                                <FormControl isInvalid={'carnet1' in errors}>
                                    <FormControl.Label _text={styles.labelInput}>Carnet Alumno 1</FormControl.Label>
                                    <Input _focus={styles.inputSeleccionado} variant="filled" keyboardType='numeric' placeholder='Digite un apellido'
                                        value={values.carnet1}
                                        onChangeText={handleChange('carnet1')}
                                        onBlur={() => setFieldTouched('carnet1')} />
                                    {touched.carnet1 && errors.carnet1 &&
                                        <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                            {errors.carnet1}
                                        </FormControl.ErrorMessage>
                                    }
                                </FormControl>
                                <FormControl isInvalid={'carnet2' in errors}>
                                    <FormControl.Label _text={styles.labelInput}>Carnet Alumno 2</FormControl.Label>
                                    <Input _focus={styles.inputSeleccionado} variant="filled" keyboardType='numeric' placeholder='Digite un apellido'
                                        value={values.carnet2}
                                        onChangeText={handleChange('carnet2')}
                                        onBlur={() => setFieldTouched('carnet2')} />
                                    {touched.carnet2 && errors.carnet2 &&
                                        <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                                            {errors.carnet2}
                                        </FormControl.ErrorMessage>
                                    }
                                </FormControl>
                                <Button size="lg" mt={5} variant="outline" _disabled={styles.botonDisabled} mb={4} onPress={handleSubmit}>Mostrar Info</Button>
                            </VStack>

                        )}
                    </Formik>
                </Box>
            </ScrollView>
        </NativeBaseProvider>
    );

}

const styles = {
    inputSeleccionado: {
        bg: "coolGray.200:alpha.100"
    },
    botonDisabled: {
        backgroundColor: '#00aeef'
    },
    labelInput: {
        color: 'black',
        fontSize: 'sm',
        fontWeight: 'bold'
    },
    tituloForm: {
        color: '#8796FF'
    }
}