import React, { useState } from 'react';
import { View, ImageBackground, StyleSheet, Text, SafeAreaView, Image, TextInput, Button } from 'react-native';

const image = { uri: 'https://reactjs.org/logo-og.png' };


const FixedDimensionsBasic = () => {
  const [correo, onChangeEmail] = useState("Nombre de usuario o contrasena");
  const [contrasena, onChangePassword] = useState("123456");
  return (
    <View style={styles.container}>

      <SafeAreaView style={styles.test}>
        <Image
          style={styles.logo}
          source={image}
        />
        <Text style={{...styles.text, paddingTop:25}}>Nombre de usuario o correo</Text>
        <TextInput style={styles.input} placeholder="Nombre de Usuario" />
        <Text style={{ ...styles.text, paddingTop: 25, fontSize: 15, fontWeight: "bold", textAlign: "left" }}>Contrasena</Text>
        <TextInput style={styles.input} placeholder="Contrasena" />
        <Text style={{ paddingTop: 25, fontSize: 15, fontWeight: "bold", textAlign: "right", alignContent: "flex-end" }}>Olvido su usuario o clave</Text>
        <Button
          title='INGRESAR'
          style = {styles.boton}/>
          <Text style={{ paddingTop: 250, fontSize: 15, fontWeight: "bold", textAlign: "right", alignContent: "flex-end" }}>Geovanny Martinez - 034519</Text>
      </SafeAreaView>

    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  test: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    paddingBottom: '50px'
  },
  input: {
    height: 40,
    width: '50%',
    borderWidth: 1,

  },
  text: {
    fontSize: 15,
    fontWeight: "bold",
    textAlign: "left"
  },
  boton: {
    padding: 10
  }
});
export default FixedDimensionsBasic;