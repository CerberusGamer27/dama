// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import {
  NativeBaseProvider, Image, Button, Text, Input, VStack, Heading, Box,
  Divider,
  FormControl,
  Link,
  Icon,
  IconButton,
  HStack,
} from 'native-base';
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';

export default function App() {
  return (
    <NativeBaseProvider>
      <Box SafeArea flex={1} p={2} w="90%" mt='10' mx='auto' paddingTop={50}>
        <VStack space={2} mt={5}>
          <Heading fontSize={40} bold>
            Bienvenido
          </Heading>
          <Heading color={"gray.300"} size="sm">
            Identifiquese para continuar!
          </Heading>

        </VStack>
        <VStack space={2} fontSize="xs" mt={5}>
          <FormControl>
            <FormControl.Label _text={{ color: 'muted.700', fontSize: 'sm', fontWeight: 600 }}>
              Correo electrónico
            </FormControl.Label>
            <Input name="txtCorreo" />
          </FormControl>
          <FormControl>
            <FormControl.Label _text={{
              color: 'muted.700', fontSize: 'sm', fontWeight: 600
            }}>Contraseña:
            </FormControl.Label>
            <Input type="password" name="txtClave" />
            <Link _text={{
              fontSize: 'xs', fontWeight: '700', color: 'cyan.500'
            }}
              alignSelf="flex-end"
              mt={1}>Olvido su contraseña?</Link>
          </FormControl>
          <VStack space={2}>
            <Button colorScheme="cyan" _text={{ color: 'white' }}>Ingresar</Button>
          </VStack>
        </VStack>
        <HStack justifyContent={'center'} paddingBottom={5} style={{position: 'absolute', left: 0, right: 0, bottom: 0}}>
          <Text fontSize='sm' color='muted.700' fontWeight={400}>Nuevo usuario? </Text>
          <Link
            _text={{ color: 'cyan.500', bold: true, fontSize: 'sm' }}
            href="#">
            Registrese aqui
          </Link>
        </HStack>
      </Box>
    </NativeBaseProvider>
  );
}
const styles = StyleSheet.create({
});
