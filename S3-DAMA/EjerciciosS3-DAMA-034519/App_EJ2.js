// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import {NativeBaseProvider, Image, Button, Text, Input, VStack, Heading, Box,  Divider} from 'native-base';

export default function App() {
  return (
    <NativeBaseProvider>
      <Box SafeArea flex={1} p={2} w="90%" mt='10' mx='auto' paddingTop={50}>
        <VStack space={2} fontSize="xs">
          <Heading size="lg" color="primary.500">Native Base</Heading>
          <Text>Native-Base es una biblioteca de componentes que permite a 
            los desarrolladores crear sistemas de diseño universal. 
            Está construido sobre React Native, lo que le permite 
            desarrollar aplicaciones para Android, iOS y la Web
          </Text>
          <Divider my={2} />
          <Button mt='3' shadow={5}>Ingresar</Button>
        </VStack>
      </Box>
    </NativeBaseProvider>
  );
}
const styles = StyleSheet.create({
});
