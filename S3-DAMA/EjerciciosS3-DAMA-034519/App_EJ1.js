import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import {NativeBaseProvider, Image, Button, Text, Input} from 'native-base';

export default function App() {
  return (
    <NativeBaseProvider>
      <View style={styles.container}>
        <Image
          borderRadius={100}
          size={150}
          alt={"Profile Picture"}
          accessibilityLabel={"Profile Picture"}
          source={{uri: 'https://www.tuexperto.com/wp-content/uploads/2015/07/perfil_01.jpg'}}
        />
        <Text style={styles.texto}>Hola Mundo</Text>
        <Button>Native-Base</Button>
        <StatusBar style="auto" />
      </View>
    </NativeBaseProvider>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  texto: {
    paddingTop: 10,
    fontSize:30,
    textAlign: 'center',
    margin: 10, color:'blue'
  },
});
