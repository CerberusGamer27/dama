#!/usr/bin/bash
if [ -z "$1" ]
  then
    echo -e "Ingrese el parametro nombreProyecto \n"
    echo -e "Ejemplo: \n ./scriptNative.sh nombreProyecto"
else
    echo -e "\nIniciando Proyecto con el nombre $1"
    expo init $1 --template @native-base/expo-template
    echo -e "\nCambiando al directorio del proyecto $1"
    cd $1
    pwd
    echo -e "\n---INICIANDO INSTALACION DE PAQUETES----\n"
    echo -e "\nInstalando Formik... \n"
    npm install formik --save
    echo -e "\nFormik Instalado Correctamente \n"
    echo -e "\n\nConfigurando los Permisos Node... \n"
    npm config set legacy-peer-deps true
    echo -e "\nPermisos Configurados Correctamente \n"
    echo -e "\n\nInstalando styled-components... \n"
    npm install styled-components styled-system
    echo -e "\nstyled-components instalado correctamente \n"
    echo -e "\n\nInstalando expo-font... \n"
    expo install expo-font
    echo -e "\n\nInstalando yup... \n"
    npm install yup --save
    echo -e "\n\nyup Instalado correctamente \n"
    echo -e "\n\nRunning expo doctor --fix-dependencies "
    expo doctor --fix-dependencies
    echo -e "\n\nTODOS LOS PAQUETES Y PERMISOS HAN SIDO CONFIGURADOS DE MANERA CORRECTA \n"
    echo -e "\n\nIniciando Proyecto \n\n"
    expo start
fi
