import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

// Views Import
import ListProducts from "./views/ListProducts";
import CreateProduct from "./views/CreateProduct";


const Stack = createNativeStackNavigator();

export default function App() {
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Lista de Productos" component={ListProducts} />
        <Stack.Screen name="CreateProduct"  component={CreateProduct} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}