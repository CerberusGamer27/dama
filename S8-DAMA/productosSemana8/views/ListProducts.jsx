import React, { useState } from 'react';
import { NativeBaseProvider, VStack, Box, Heading, FlatList, Spacer, Icon, Stack, HStack, StyleSheet, Text, View, Button, ScrollView } from 'native-base';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons, FontAwesome } from '@expo/vector-icons';

import { collection, getDocs } from "firebase/firestore";
import db from '../config/firebase';

export default function ListUser({ navigation }) {
  const [productos, setProductos] = useState(null)

  useFocusEffect(
    React.useCallback(() => {
      //Creando función asíncrona 
      const obtenerDatos = async () => {
        //getDocs: Ejecuta la consulta y devuelve los resultados como una QuerySnapshot
        //QuerySnapshot: Contiene el resultado de una consulta
        //collection: Obtiene una instancia a la coleccion especificada
        let products = [];
        const q = collection(db, 'products')
        const datos = await getDocs(q);

        datos.forEach((documento) => {
          products.push(documento.data());
        });
        console.log(products);
        setProductos(products)
      }
      //Llamado de la función
      obtenerDatos();
    }, [])
  );

  return (
    <NativeBaseProvider>
      <Box w={"95%"} mt={5} flex={1} p={1} marginLeft={1}>
        <Heading fontSize="xl" p="4" pb="3" textAlign={'center'}>
          Listado de Productos
        </Heading>
        <FlatList data={productos} renderItem={({ item }) =>
          <Box borderBottomWidth="1" _dark={{ borderColor: "muted.50" }}
            borderColor="muted.800" pl={["0", "4"]} pr={["0", "5"]} py="2">
            <HStack space={2} justifyContent="space-between">
              <VStack>
                <Text _dark={{ color: "warmGray.50" }} color="coolGray.800" bold>
                  Nombre Producto: {item.nombre}
                </Text>
                <Text color="coolGray.600" _dark={{ color: "warmGray.200" }}>
                  Proveedor: {item.proveedor}
                </Text>
                <Text color="coolGray.600" _dark={{ color: "warmGray.200" }}>
                  Precio $: {item.precio}
                </Text>
              </VStack>
              <Spacer />
              <Text fontSize="xs" _dark={{
                color: "warmGray.50"
              }} color="coolGray.800" alignSelf="flex-start">
                Cant. {item.cantidad}
              </Text>
            </HStack>
          </Box>} keyExtractor={item => item.nombre} />
        <Button marginBottom={5}
          leftIcon={<Ionicons name='add-circle' size={32} color='green' />}
          onPress={() => navigation.navigate('CreateProduct')}>
          Crear Producto
        </Button>
      </Box>
    </NativeBaseProvider>
  );
}