import { React, useState } from 'react';
import { Input, Stack, FormControl, NativeBaseProvider, Box, ScrollView, Button } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
//Importando el uso de colecciones en Firebase
import { collection, addDoc } from 'firebase/firestore';
//Importar la conexión a Firebase de nuestro proyecto
import db from '../config/firebase';

export default function CreateProduct({ navigation }) {
    const initialState = {
        id_producto: "",
        nombre: "",
        cantidad: "",
        precio: "",
        proveedor: ""
    }

    //Empleando el state para poder interacturar con los elementos de la coleccion
    const [state, setState] = useState(initialState);

    //Creando función para captura de valores por teclado
    const handleChangeText = (name, value) => {
        setState({ ...state, [name]: value });
    };

    //Agregando funcion asincrona para nuevo usuario
    const saveNewProduct = async () => {
        try {
            //addDoc: Agrega un nuevo documento a la coleccion referenciada como 
            //parámetro de collection.
            //with the given data, assigning it a document ID automatically.
            await addDoc(collection(db, "products"), {
                //Tomando el valor del state para la colección
                id_producto: state.id_producto,
                nombre: state.nombre,
                cantidad: state.cantidad,
                precio: state.precio,
                proveedor: state.proveedor
            });

            //Redireccionando a la pantalla de "Lista de usuarios"
            navigation.navigate('Lista de Productos');
        } catch (error) {
            console.log(error);
        }
    };
    return (
        <NativeBaseProvider>
            <ScrollView>
                <Box w={"95%"} mt={5} flex={1} p={1} marginLeft={1}>
                    <FormControl>
                        <Stack space={5}>
                            <Stack>
                                <FormControl.Label>ID Producto</FormControl.Label>
                                <Input variant="underlined" keyboardType='number-pad' p={2} placeholder="ID Aqui"
                                onChangeText={(value) => handleChangeText("id_producto", value)}
                                value={state.id_producto}
                                />
                            </Stack>
                            <Stack>
                                <FormControl.Label>Nombre Producto</FormControl.Label>
                                <Input variant="underlined" p={2} placeholder="Nombre Aqui"
                                onChangeText={(value) => handleChangeText("nombre", value)}
                                value={state.nombre}
                                />
                            </Stack>
                            <Stack>
                                <FormControl.Label>Cantidad Producto</FormControl.Label>
                                <Input variant="underlined" keyboardType='number-pad' p={2} placeholder="Cantidad Aqui"
                                onChangeText={(value) => handleChangeText("cantidad", value)}
                                value={state.cantidad}
                                />
                            </Stack>
                            <Stack>
                                <FormControl.Label>Precio Producto</FormControl.Label>
                                <Input variant="underlined" keyboardType='decimal-pad' p={2} placeholder="Precio Aqui"
                                onChangeText={(value) => handleChangeText("precio", value)}
                                value={state.precio}
                                />
                            </Stack>
                            <Stack>
                                <FormControl.Label>Proveedor Producto</FormControl.Label>
                                <Input variant="underlined" p={2} placeholder="Proveedor Aqui"
                                onChangeText={(value) => handleChangeText("proveedor", value)}
                                value={state.proveedor}
                                />
                            </Stack>
                        </Stack>
                        <Button marginBottom={5} marginTop={5}
                            leftIcon={<Ionicons name='add-circle' size={32} color='green' />}
                            onPress={
                                () => saveNewProduct()
                            }>
                            Crear Producto
                        </Button>
                    </FormControl>
                </Box>
            </ScrollView>
        </NativeBaseProvider>
    );
};

