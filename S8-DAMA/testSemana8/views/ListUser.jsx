import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

//Importando elementos de interacción con las colecciones
import { collection, getDocs } from "firebase/firestore";
//Importando el componente de conexión a Firebase
import db from '../database/firebase';

export default function ListUser({ navigation }) {
  //useEffect: es un hook que permite ejecutar código cada vez que
  //nuestro componente se renderice 
  //(ya sea por una actualización o sea la primera vez)

  // Esta forma no funciona en react native ya que el componente no se renderiza
  /*useEffect(() => {
    //Creando función asíncrona 
    const obtenerDatos = async () => {
      //getDocs: Ejecuta la consulta y devuelve los resultados como una QuerySnapshot
      //QuerySnapshot: Contiene el resultado de una consulta
      //collection: Obtiene una instancia a la coleccion especificada
      const datos = await getDocs(collection(db, 'users'));
      datos.forEach((documento) => {
        //Mostrando en consola el resultado
        //data: Recupera todos los campos del documento como un objeto.
        console.log(documento.data());
      });
    }
    //Llamado de la función
    obtenerDatos();
  }, []);*/

  // Esta forma si funciona en react native, ya que el navigation afecta la forma en la que se renderiza
  // Un componente
  useFocusEffect(
  React.useCallback(() => {
    //Creando función asíncrona 
    const obtenerDatos = async () => {
      //getDocs: Ejecuta la consulta y devuelve los resultados como una QuerySnapshot
      //QuerySnapshot: Contiene el resultado de una consulta
      //collection: Obtiene una instancia a la coleccion especificada
      const datos = await getDocs(collection(db, 'users'));
      datos.forEach((documento) => {
        //Mostrando en consola el resultado
        //data: Recupera todos los campos del documento como un objeto.
        console.log(documento.data());
      });
    }
    //Llamado de la función
    obtenerDatos();
  }, [])
);

  return (
    <View>
      <Text style={styles.Text}>Listado de usuarios</Text>
      <Button title="Crear usuario"
        onPress={() => navigation.navigate('CreateUser')} />
    </View>


  );
}

const styles = StyleSheet.create({
  Text: {
    fontSize: 25,
    color: 'cyan'
  }
})