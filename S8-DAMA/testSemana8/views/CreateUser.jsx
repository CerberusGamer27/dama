import React, { useState, props } from 'react';
import { StyleSheet, Button, TextInput, ScrollView, View } from 'react-native';
//Importando el uso de colecciones en Firebase
import { collection, addDoc } from 'firebase/firestore';
//Importar la conexión a Firebase de nuestro proyecto
import db from '../database/firebase';

//Agregar Navegacion a la pantalla con "navigation"
export default function CreateUser({ navigation }) {
  //Crear la estructura de nuestra colección (tabla)
  const initialState = {
    name: "",
    email: "",
    phone: "",
  };
  //Empleando el state para poder interacturar con los elementos de la coleccion
  const [state, setState] = useState(initialState);

  //Creando función para captura de valores por teclado
  const handleChangeText = (name, value) => {
    setState({ ...state, [name]: value });
  };

  //Agregando funcion asincrona para nuevo usuario
  const saveNewUser = async () => {
    try {
      //addDoc: Agrega un nuevo documento a la coleccion referenciada como 
      //parámetro de collection.
      //with the given data, assigning it a document ID automatically.
      await addDoc(collection(db, "users"), {
        //Tomando el valor del state para la colección
        name: state.name,
        email: state.email,
        phone: state.phone
      });

      //Redireccionando a la pantalla de "Lista de usuarios"
      navigation.navigate('Lista de Usuarios');
    } catch (error) {
      console.log(error);
    }
  };
  //Diseñando la interface de captura de datos 
  return (
    <ScrollView style={styles.container}>
      <View style={styles.inputGroup}>
        <TextInput placeholder="Nombre de usuario"
          onChangeText={(value) => handleChangeText("name", value)}
          value={state.name}
        />
      </View>
      <View style={styles.inputGroup}>
        <TextInput placeholder="Email de usuario"
          onChangeText={(value) => handleChangeText("email", value)}
          value={state.email}
        />
      </View>
      <View style={styles.inputGroup}>
        <TextInput placeholder="Telefono de usuario"
          onChangeText={(value) => handleChangeText("phone", value)}
          value={state.phone}

        />
      </View>
      <View>
        < Button title="Guardar registro"
          onPress={
            () => saveNewUser()
          }
        />
      </View>
    </ScrollView>
  );
}
//Definición de estilos empleados en la pantalla
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc'
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  }
})