import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Primera Aplicacion React Native - Ejemplo</Text>
      <Text>Nombre: Geovanny Roberto Martinez Avila</Text>
      <Text>Carne: 034519</Text>
      <Text>Seccion: DS39</Text>
      <Text>Edad: 21</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
