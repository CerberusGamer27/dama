// import { StatusBar } from 'expo-status-bar';
import { StyleSheet } from 'react-native';
import {
  NativeBaseProvider, Button, Input, VStack, Heading, Box,
  FormControl,
  HStack,
  Avatar,
  InputGroup,
  InputLeftAddon,
  ScrollView,
} from 'native-base';
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';

export default function App() {
  return (
    <NativeBaseProvider>
      <ScrollView>
        <Box SafeArea flex={1} p={2} w="90%" mt='10' mx='auto' paddingTop={50}>
          <HStack space={2} mt={5} justifyContent="space-between">
            <Heading size="md" bold>
              Bienvenido Empleado
            </Heading>
            {/* <Image size={75} borderRadius={100} source={{
              uri: "https://wallpaperaccess.com/full/317501.jpg"
            }} alt="Alternate Text" /> */}
            <Avatar size="xl" source={{
              uri: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/81c743dd-60e4-49b6-91ed-d6849a6491bc/daepq9j-b624be25-310e-48fe-aa3a-7a8a10284c87.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzgxYzc0M2RkLTYwZTQtNDliNi05MWVkLWQ2ODQ5YTY0OTFiY1wvZGFlcHE5ai1iNjI0YmUyNS0zMTBlLTQ4ZmUtYWEzYS03YThhMTAyODRjODcuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.d7yM98GG-CA84EbIrKqo2PelFaql35NNojDI9Wna5r4"}}>
                GG
                <Avatar.Badge bg={"green.500"} />
            </Avatar> 
          </HStack>
          <VStack space={2} fontSize="xs" mt={5}>
            <FormControl>
              <FormControl.Label _text={{ color: 'muted.700', fontSize: 'sm', fontWeight: 600 }}>
                Nombre Empleado
              </FormControl.Label>
              <Input name="txtNombre" variant={"rounded"} placeholder="Tu Nombre aqui" />
            </FormControl>

            <FormControl>
              <FormControl.Label _text={{ color: 'muted.700', fontSize: 'sm', fontWeight: 600 }}>
                Apellido Empleado
              </FormControl.Label>
              <Input name="txtApellido" variant={"rounded"} placeholder="Tu Apellido aqui" />
            </FormControl>

            <FormControl>
              <FormControl.Label _text={{ color: 'muted.700', fontSize: 'sm', fontWeight: 600 }}>
                Direccion
              </FormControl.Label>
              <Input name="txtDireccion" variant={"rounded"} placeholder="Tu Direccion aqui" />
            </FormControl>

            <FormControl>
              <FormControl.Label _text={{ color: 'muted.700', fontSize: 'sm', fontWeight: 600 }}>
                Correo
              </FormControl.Label>
              <Input name="txtCorreo" keyboardType='email-address'
              type='text' variant={"rounded"} placeholder="Tu Correo Aqui" />
            </FormControl>

            <FormControl>
              <FormControl.Label _text={{ color: 'muted.700', fontSize: 'sm', fontWeight: 600 }}>
                Salario
              </FormControl.Label>
              <InputGroup w={{base: "95%"}}>
                <InputLeftAddon children={"$"} />
                <Input w={{base: "100%"}} keyboardType="numeric" 
                variant={"rounded"} placeholder="Tu Salario Aqui" />
              </InputGroup>
            </FormControl>
            
            <VStack space={2} mt='10'>
              <Button colorScheme="cyan" _text={{ color: 'white' }}>Registrar</Button>
            </VStack>
          </VStack>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}